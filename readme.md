# Arbor - Frontend Interview Assignment

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes

### Prerequisites

```
npm
```

### Installing

```
Download source code from master branch
Run "npm install"
```

## Running

```
Run "npm run dev"
Open 'http://localhost:3001/' in your browser
```

## Built With

* [React](https://reactjs.org/) - The web framework used
* [Redux](https://redux.js.org/) - State container
* [Node Package Manager](https://nodejs.org/en/) - Dependency Management
* [webpack](https://webpack.js.org/) - Automation tool
* [SASS](https://sass-lang.com/) - CSS preprocessor

## Author

* **Denis Bojanic**