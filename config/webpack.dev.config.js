var webpack = require("webpack");
var path = require("path");

var postcssModulesValues = require("postcss-modules-values");
var autoprefixer = require("autoprefixer");

var parentDir = path.join(__dirname, "../");

module.exports = {
  watch: true,
  watchOptions: {
    poll: true
  },
  entry: ["regenerator-runtime/runtime", path.join(__dirname, "../src/init/index")],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loaders: ["babel-loader", "eslint-loader"]
      },
      {
        test: /\.jpg/,
        exclude: /node_modules/,
        loader: "file-loader"
      },
      {
        test: /\.svg$/,
        exclude: /node_modules/,
        use: {
          loader: "svg-react-loader"
        }
      },
      {
        test: /\.(scss|css)$/,
        exclude: [/node_modules/],
        include: [/src/],
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
              modules: true,
              localIdentName: "[name]__[local]--[hash:base64:5]"
            }
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss", // https://webpack.js.org/guides/migrating/#complex-options
              plugins: () => [
                require("postcss-flexbugs-fixes"),
                postcssModulesValues(),
                autoprefixer({
                  browsers: [
                    ">1%",
                    "last 5 versions",
                    "Firefox ESR",
                    "not ie < 9" // React doesn't support IE8 anyway
                  ],
                  flexbox: "no-2009"
                })
              ]
            }
          },
          {
            loader: "sass-loader"
          }
        ]
      },

      {
        test: /\.(css)$/,
        include: [/node_modules/],
        use: [
          {
            loader: "style-loader" // creates style nodes from JS strings
          },
          {
            loader: "css-loader" // translates CSS into CommonJS
          }
        ]
      }
    ]
  },
  output: {
    path: parentDir + "/dist",
    filename: "bundle.js"
  },
  devServer: {
    contentBase: parentDir,
    port: 3001,
    historyApiFallback: true
  }
};
