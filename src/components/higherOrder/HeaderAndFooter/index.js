import React from "react";
import Header from "../../common/Header";
import Footer from "../../common/Footer";
import styles from "./style.scss";
/**
 * Higher-order component which adds standard header and footer
 * mode.
 */
export function withStandardHeaderAndFooter(Container) {
  return (props => (
    <div className={styles.Container}>
      <Header />
      <div className={styles.ContainerItem}>
        <div className="Site">
          <main className="Site-content">
            <Container {...props} />
          </main>
        </div>
      </div>
      <Footer />
    </div>
  ))();
}
