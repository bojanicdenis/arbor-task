import React from "react";
import PropTypes from "prop-types";

import styles from "./attendanceList.scss";

import AttendanceListItem from "./attendanceListItem";

const Attendance = ({ attendanceList, markAttendance, markSelected }) => {
  return (
    <div className={styles.attendance}>
      {attendanceList.map(attendanceItem => {
        return (
          <AttendanceListItem
            attendanceItem={attendanceItem}
            markAttendance={markAttendance}
            markSelected={markSelected}
            key={attendanceItem.id}
          />
        );
      })}
    </div>
  );
};

Attendance.propTypes = {
  attendanceList: PropTypes.array,
  markAttendance: PropTypes.func.isRequired,
  markSelected: PropTypes.func.isRequired
};

export default Attendance;
