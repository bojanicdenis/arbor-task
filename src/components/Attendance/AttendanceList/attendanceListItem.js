import React from "react";
import PropTypes from "prop-types";
import {
  ATTENDANCE_PRESENT,
  ATTENDANCE_LATE,
  ATTENDANCE_ABSENT
} from "../../../containers/Attendance/attendanceConstants";

import Present from "./icons/checked.svg";
import Late from "./icons/hourglass.svg";
import Absent from "./icons/cross.svg";

import styles from "./attendanceList.scss";

function requireImage(imagePath) {
  try {
    return require(`${imagePath}`);
  } catch (e) {
    return;
  }
}

const attendanceActions = [
  {
    attendance: ATTENDANCE_PRESENT,
    label: "Present",
    icon: <Present className={styles.attendanceIcons} />
  },
  {
    attendance: ATTENDANCE_LATE,
    label: "Late",
    icon: <Late className={styles.attendanceIcons} />
  },
  {
    attendance: ATTENDANCE_ABSENT,
    label: "Absent",
    icon: <Absent className={styles.attendanceIcons} />
  }
];

const AttendanceListItem = ({
  attendanceItem,
  markAttendance,
  markSelected
}) => {
  const onMarkSelected = function() {
    markSelected(attendanceItem.id);
  };
  const name = `${attendanceItem.firstName} ${attendanceItem.lastName}`;
  const selectedClass = attendanceItem.selected ? styles.attendeeSelected : "";
  return (
    <div className={`${styles.attendee} ${selectedClass}`}>
      <div onClick={onMarkSelected}>
        <img
          className={styles.atendeeImage}
          src={requireImage(attendanceItem.image)}
          alt={name}
        />
      </div>
      <div className={styles.attendeeInfo}>
        <h4 onClick={onMarkSelected} className={styles.attendeeName}>
          {name}
        </h4>
        <div className={styles.attendanceIconDiv}>
          {!attendanceItem.shortMark &&
            attendanceActions.map(attendanceAction => {
              return (
                <span
                  className={styles.attendanceIconContainer}
                  key={attendanceAction.attendance}
                  title={attendanceAction.label}
                  onClick={markAttendance(
                    attendanceItem.id,
                    attendanceAction.attendance
                  )}
                >
                  {attendanceAction.icon}
                </span>
              );
            })}
          {!!attendanceItem.shortMark && (
            <div className={styles.attendancePicked}>
              {attendanceActions[attendanceItem.shortMark - 1].icon}{" "}
              <div>{attendanceActions[attendanceItem.shortMark - 1].label}</div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

AttendanceListItem.propTypes = {
  attendanceList: PropTypes.array,
  markAttendance: PropTypes.func.isRequired,
  markSelected: PropTypes.func.isRequired
};

export default AttendanceListItem;
