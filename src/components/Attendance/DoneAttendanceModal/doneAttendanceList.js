import React from "react";
import PropTypes from "prop-types";
import styles from "./doneAttendanceModal.scss";

const DoneAttendanceList = ({ header, list }) => {
  return (
    <div className={styles.attendanceListWrapper}>
      <h3 className={styles.attendanceListHeader}>
        {header}{" "}
        <span className={styles.attendanceListLength}>({list.length})</span>
      </h3>
      {list.length > 0 &&
        list.map(attendance => {
          return (
            <div key={attendance.id} className={styles.attendeeName}>
              {attendance.firstName} {attendance.lastName}
            </div>
          );
        })}
      {list.length === 0 && `No one is marked as ${header.toLowerCase()}`}
    </div>
  );
};

DoneAttendanceList.propTypes = {
  header: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired
};

export default DoneAttendanceList;
