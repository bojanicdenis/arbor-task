import React from "react";
import PropTypes from "prop-types";
import DoneAttendanceList from "./doneAttendanceList";
import Modal from "../../common/Modal";
import {
  ATTENDANCE_PRESENT,
  ATTENDANCE_LATE,
  ATTENDANCE_ABSENT
} from "../../../containers/Attendance/attendanceConstants";
import string from "../../../localization";

const Index = ({ attendanceList, onClose, modalOpened }) => {
  let attendancePresent = attendanceList.filter(
    attendance => attendance.shortMark === ATTENDANCE_PRESENT
  );
  let attendanceLate = attendanceList.filter(
    attendance => attendance.shortMark === ATTENDANCE_LATE
  );
  let attendanceAbsent = attendanceList.filter(
    attendance => attendance.shortMark === ATTENDANCE_ABSENT
  );

  return (
    <div>
      <Modal
        isOpen={modalOpened}
        title={string.finalAttendanceList}
        contentLabel={string.finalAttendanceList}
        rightButtonLabel={string.ok}
        onRightButtonClick={onClose}
        onClose={onClose}
      >
        <div>
          <DoneAttendanceList header={string.present} list={attendancePresent} />
          <DoneAttendanceList header={string.late} list={attendanceLate} />
          <DoneAttendanceList header={string.absent} list={attendanceAbsent} />
        </div>
      </Modal>
    </div>
  );
};

Index.propTypes = {
  attendanceList: PropTypes.array,
  onClose: PropTypes.func.isRequired,
  modalOpened: PropTypes.bool.isRequired
};

export default Index;
