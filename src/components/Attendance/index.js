import React from "react";

import AttendanceButtons from "./AttendanceMainButtons";
import AttendanceList from "./AttendanceList";
import BundleActionModal from "./BundleActionModal";
import DoneAttendanceModal from "./DoneAttendanceModal";
import AttendanceCounterWrapper from "./AttendanceCounter";
import PropTypes from "prop-types";

const Attendance = ({
  attendanceList,
  attendanceCounter,
  doneModalOpened,
  bundleActionModalOpened,
  selectedAction,
  bundleActionSelect,
  bundleAction,
  markAttendance,
  markSelected,
  toggleBundleModal,
  toggleDoneModal,
  resetAttendance
}) => {
  return (
    <div>
      <BundleActionModal
        onClose={toggleBundleModal}
        modalOpened={bundleActionModalOpened}
        bundleActionSelect={bundleActionSelect}
        bundleAction={bundleAction}
        attendanceList={attendanceList}
        selectedAction={selectedAction}
      />
      <DoneAttendanceModal
        attendanceList={attendanceList}
        onClose={toggleDoneModal}
        modalOpened={doneModalOpened}
      />
      <AttendanceList
        attendanceList={attendanceList}
        markAttendance={markAttendance}
        markSelected={markSelected}
      />
      <AttendanceCounterWrapper attendanceCounter={attendanceCounter} />
      <AttendanceButtons
        openBundleModal={toggleBundleModal}
        resetAttendance={resetAttendance}
        attendanceList={attendanceList}
        attendanceCounter={attendanceCounter}
        doneAttendance={toggleDoneModal}
      />
    </div>
  );
};


Attendance.propTypes = {
    attendanceList: PropTypes.array,
    attendanceCounter: PropTypes.object,
    doneModalOpened: PropTypes.bool.isRequired,
    bundleActionModalOpened: PropTypes.bool.isRequired,
    selectedAction: PropTypes.number,
    bundleActionSelect: PropTypes.func.isRequired,
    bundleAction: PropTypes.func.isRequired,
    markAttendance: PropTypes.func.isRequired,
    markSelected: PropTypes.func.isRequired,
    toggleBundleModal: PropTypes.func.isRequired,
    toggleDoneModal: PropTypes.func.isRequired,
    resetAttendance: PropTypes.func.isRequired,
};


export default Attendance;
