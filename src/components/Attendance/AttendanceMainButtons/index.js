import React from "react";
import PropTypes from "prop-types";
import styles from "./attendanceButtons.scss";
import string from "../../../localization";
import Button from "../../common/Button";

const AttendanceButtonsGroup = ({
  openBundleModal,
  resetAttendance,
  doneAttendance,
  attendanceCounter,
  attendanceList
}) => {
  const hasMarked =
    attendanceCounter.present ||
    attendanceCounter.late ||
    attendanceCounter.absent;
  const hasSelected = attendanceList.find(({ selected }) => selected);
  return (
    <div className={styles.buttonWrapper}>
      {hasSelected !== undefined && (
        <Button
          onClick={openBundleModal}
          color={"INFO"}
          label={string.markSelected}
        />
      )}
      {!!hasMarked && (
        <Button
          onClick={resetAttendance}
          color={"NEGATIVE"}
          label={string.resetAll}
        />
      )}
      {!!hasMarked && (
        <Button onClick={doneAttendance} className={"button"} label={string.done} />
      )}
    </div>
  );
};

AttendanceButtonsGroup.propTypes = {
  attendanceList: PropTypes.array,
  openBundleModal: PropTypes.func.isRequired,
  resetAttendance: PropTypes.func.isRequired,
  doneAttendance: PropTypes.func.isRequired,
  attendanceCounter: PropTypes.object.isRequired
};

export default AttendanceButtonsGroup;
