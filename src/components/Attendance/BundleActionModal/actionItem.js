import React from "react";
import PropTypes from "prop-types";
import styles from "./bundleActionModal.scss";

const ActionItem = ({ action, selected, label, bundleActionSelect }) => {
  const className = selected ? styles.selected : "";
  return (
    <div
      role={"button"}
      tabIndex={0}
      className={`${styles.actionItem} ${className}`}
      onClick={bundleActionSelect(action)}
    >
      {label}
    </div>
  );
};

ActionItem.propTypes = {
  action: PropTypes.number.isRequired,
  bundleActionSelect: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired
};

export default ActionItem;
