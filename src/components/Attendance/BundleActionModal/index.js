import React from "react";
import PropTypes from "prop-types";
import Modal from "../../common/Modal";
import styles from "./bundleActionModal.scss";
import ActionItem from "./actionItem";
import {
  ATTENDANCE_NOT_MARKED,
  ATTENDANCE_PRESENT,
  ATTENDANCE_LATE,
  ATTENDANCE_ABSENT
} from "../../../containers/Attendance/attendanceConstants";
import string from "../../../localization";

const bundleActions = [
  { action: ATTENDANCE_NOT_MARKED, label: string.notMarked },
  { action: ATTENDANCE_PRESENT, label: string.present },
  { action: ATTENDANCE_LATE, label: string.late },
  { action: ATTENDANCE_ABSENT, label: string.absent }
];

const BundleActionModal = ({
  onClose,
  modalOpened,
  bundleActionSelect,
  selectedAction,
  bundleAction,
  attendanceList
}) => {
  let selectedAttendeesList = [];
  let selectedAttendeesNames = "";
  if (modalOpened) {
    selectedAttendeesList = attendanceList.filter(
      attendee => attendee.selected
    );
    selectedAttendeesNames = selectedAttendeesList
      .map(({ firstName, lastName }) => {
        return `${firstName} ${lastName}`;
      })
      .join(", ");
  }
  return (
    <div>
      <Modal
        isOpen={modalOpened}
        title={string.bundleActions}
        contentLabel={string.markSelected}
        rightButtonLabel={string.apply}
        onRightButtonClick={bundleAction}
        onClose={onClose}
      >
        <div>
          <span className={styles.infoText}>
            Mark {selectedAttendeesList.length} selected attendees (
            <i>{selectedAttendeesNames}</i>) as:{" "}
          </span>
          {bundleActions.map(action => {
            return (
              <ActionItem
                key={action.action}
                bundleActionSelect={bundleActionSelect}
                label={action.label}
                action={action.action}
                selected={selectedAction === action.action}
              />
            );
          })}
        </div>
      </Modal>
    </div>
  );
};

BundleActionModal.propTypes = {
  attendanceList: PropTypes.array,
  onClose: PropTypes.func,
  modalOpened: PropTypes.bool,
  bundleActionSelect: PropTypes.func.isRequired,
  selectedAction: PropTypes.number,
  bundleAction: PropTypes.func.isRequired
};

export default BundleActionModal;
