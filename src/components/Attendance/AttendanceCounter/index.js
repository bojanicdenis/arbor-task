import React from "react";
import styles from "./markCounter.scss";
import AttendanceCounterItem from "./AttendanceCounterItem";
import PropTypes from "prop-types";
import string from "../../../localization";

const attendanceMarks = [
  { type: "notMarked" },
  { type: "present" },
  { type: "late" },
  { type: "absent" }
];

const AttendanceCounter = ({ attendanceCounter }) => {
  return (
    <div className={styles.counterDiv}>
      <div className={styles.divHeader}>{string.attendanceCount}</div>
      {attendanceMarks.map(({ type }) => (
        <AttendanceCounterItem
          key={type}
          count={attendanceCounter[type]}
          type={string[type]}
        />
      ))}
    </div>
  );
};

AttendanceCounter.propTypes = {
  attendanceCounter: PropTypes.object.isRequired
};

export default AttendanceCounter;
