import React from "react";
import PropTypes from "prop-types";
import styles from "./markCounter.scss";

const AttendanceCounterItem = ({ count, type }) => {
  return (
    <div className={styles.counterRow}>
      <span className={styles.counterLabel}>{type}:</span>{" "}
      <span className={styles.counterAmount}>{count}</span>
    </div>
  );
};

AttendanceCounterItem.propTypes = {
  count: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired
};

export default AttendanceCounterItem;
