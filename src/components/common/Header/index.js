import React from "react";
import styles from "./header.scss";
import string from "../../../localization";

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={`${styles.headerParagraph}`}>
        Arbor - {string.frontendAssignement}
      </div>
    </header>
  );
};

export default Header;
