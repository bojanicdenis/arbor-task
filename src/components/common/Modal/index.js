import React, { Component } from "react";
import PropTypes from "prop-types";
import ReactModal from "react-modal";

import Button from "../Button/index";

import CloseIcon from "./close.svg";
import styles from "./modal.scss";

class Modal extends Component {
  render() {
    const {
      isOpen,
      title = "",
      titleIcon,
      contentLabel,
      rightButtonLabel,
      onClose,
      onRightButtonClick,
      footerLeftItem = null,
      noHeader = false,
      children,
      className = "",
      overflow,
      isLarge = false
    } = this.props;
    let modalClassName = styles.Content;
    if (isLarge) modalClassName += ` ${styles.LargeContent}`;

    return (
      <ReactModal
        isOpen={isOpen}
        shouldCloseOnOverlayClick={false}
        className={modalClassName}
        overlayClassName={`${styles.Overlay} ${className}`.trim()}
        contentLabel={contentLabel}
        onRequestClose={onClose}
        ariaHideApp={false}
      >
        <div onKeyUp={this.handleKeyUp}>
          {!noHeader && (
            <div className={styles.Header}>
              <span className={styles.Title}>
                {title} {titleIcon}
              </span>
              <button className={styles.CloseIconButton} onClick={onClose}>
                <CloseIcon className={styles.CloseIcon} />
              </button>
            </div>
          )}

          <div className={styles.Body} style={{ overflow }}>
            {children}
          </div>

          <div className={styles.Footer}>
            <div className={styles.Left}>{footerLeftItem}</div>
            <div className={styles.Right}>
              <Button
                className={styles.Button}
                label={rightButtonLabel}
                onClick={onRightButtonClick}
              />
            </div>
          </div>
        </div>
      </ReactModal>
    );
  }
}

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string,
  titleIcon: PropTypes.any,
  leftButtonLabel: PropTypes.string,
  rightButtonLabel: PropTypes.string.isRequired,
  onLeftButtonClick: PropTypes.func,
  onRightButtonClick: PropTypes.func.isRequired,
  middleButtonLabel: PropTypes.string,
  onMiddleButtonClick: PropTypes.func,
  onClose: PropTypes.func.isRequired,
  contentLabel: PropTypes.string.isRequired,
  footerLeftItem: PropTypes.node,
  noHeader: PropTypes.bool,
  overflow: PropTypes.string
};

export default Modal;
