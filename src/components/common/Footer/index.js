import React from "react";
import styles from "./footer.scss";
import string from "../../../localization";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <p className={`${styles.footerParagraph}`}>
        {string.madeBy}{" "}
        <a
          className={styles.link}
          href={`https://linkedin.com/in/denis-bojanic-96125064/`}
        >
          {" "}
          Bojanic Denis
        </a>
        .
      </p>
    </footer>
  );
};

export default Footer;
