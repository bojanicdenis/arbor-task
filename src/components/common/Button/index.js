import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "./button.scss";

export const BUTTON_COLORS = {
  POSITIVE: "POSITIVE",
  NEGATIVE: "NEGATIVE",
  INFO: "INFO",
  DESTRUCTIVE: "DESTRUCTIVE"
};

const colorToClassMap = {
  [BUTTON_COLORS.POSITIVE]: styles.Positive,
  [BUTTON_COLORS.NEGATIVE]: styles.Negative,
  [BUTTON_COLORS.INFO]: styles.Info,
  [BUTTON_COLORS.DESTRUCTIVE]: styles.Destructive
};

class Button extends Component {
  render() {
    let {
      label,
      type,
      color = BUTTON_COLORS.POSITIVE,
      style,
      className,
      onClick
    } = this.props;

    let btnClass = styles.button;
    if (className) btnClass += ` ${className}`;
    const colorClass = colorToClassMap[color];
    if (colorClass) btnClass += ` ${colorClass}`;
    return (
      <button
        type={type}
        className={btnClass}
        onClick={onClick}
        style={style}
        onMouseDown={this.setButtonActiveFlag}
        onMouseLeave={this.setButtonNotActiveFlag}
        onMouseUp={this.setTimerToRemoveActiveFlag}
      >
        {label}
      </button>
    );
  }
}

Button.propTypes = {
  type: PropTypes.string,
  label: PropTypes.string.isRequired,
  color: PropTypes.oneOf(Object.values(BUTTON_COLORS)),
  onClick: PropTypes.func.isRequired,
  style: PropTypes.object,
  className: PropTypes.string
};

export default Button;
