import { all } from 'redux-saga/effects';
import { getAttendanceSaga } from '../containers/Attendance/attendanceSagas';

export default function* rootSaga() {
    yield all([
        getAttendanceSaga()
    ]);
}
