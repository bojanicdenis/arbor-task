import { combineReducers } from "redux";

import attendance from "../containers/Attendance/attendanceReducer";

const rootReducer = combineReducers({
  attendance
});

export default rootReducer;
