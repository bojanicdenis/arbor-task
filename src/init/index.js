import React from "react";
import ReactDOM from "react-dom";

import { store } from "./store";
import { Provider } from "react-redux";

// main app
import App from "../containers/App/index";

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
