import React from "react";
import Attendance from "../Attendance";
import { withStandardHeaderAndFooter } from "../../components/higherOrder/HeaderAndFooter";

class App extends React.Component {
  render() {
    return withStandardHeaderAndFooter(Attendance);
  }
}

export default App;
