import mockData from './mockAttendanceData';

export function getAttendanceListApi() {
    return Promise.resolve(mockData); // Redux-saga requires promise to be returned
}