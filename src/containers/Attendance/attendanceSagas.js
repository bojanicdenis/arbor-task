import { takeLatest, put } from 'redux-saga/effects';
import {
    GET_ATTENDANCE_LIST_REQUEST,
} from './attendanceConstants';
import {
    getAttendanceListSuccess,
    getAttendanceListFailure,
} from './attendanceActions';
import { getAttendanceListApi } from './attendanceApi';

export function* getAttendanceSaga() {
    yield takeLatest(GET_ATTENDANCE_LIST_REQUEST, getAttendanceList);
}

function* getAttendanceList() {
    try {
        let attendanceList = yield getAttendanceListApi();
        yield put(getAttendanceListSuccess(attendanceList));
    } catch (e) {
        //eslint-disable-next-line
        console.warn('getAttendanceList fetch failed, please try again ...', e);
        yield put(getAttendanceListFailure());
    }
}
