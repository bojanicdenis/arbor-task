import {
  GET_ATTENDANCE_LIST_REQUEST,
  GET_ATTENDANCE_LIST_SUCCESS,
  GET_ATTENDANCE_LIST_FAILURE,
  ATTENDANCE_NOT_MARKED,
  ATTENDANCE_PRESENT,
  ATTENDANCE_LATE,
  ATTENDANCE_ABSENT
} from "./attendanceConstants";

const initialState = {
  isGetAttendanceListInProgress: false,
  isGetAttendanceListFailed: false,
  attendanceList: undefined
};

function attendanceReducer(state = initialState, action) {
  switch (action.type) {
    case GET_ATTENDANCE_LIST_REQUEST:
      return Object.assign({}, state, {
        isGetAttendanceListInProgress: true,
        isGetAttendanceListFailed: false,
        attendanceList: undefined
      });
    case GET_ATTENDANCE_LIST_SUCCESS:
      return Object.assign({}, state, {
        isGetAttendanceListInProgress: false,
        isGetAttendanceListFailed: false,
          // Set shortMark property that has numeric value; Much easier to work with throughout the app than object with multiple properties.
        attendanceList: action.attendanceList.map(attendance => {
          if (attendance.attendanceMark.present) {
            attendance.shortMark = ATTENDANCE_PRESENT;
          } else if (attendance.attendanceMark.late) {
            attendance.shortMark = ATTENDANCE_LATE;
          } else if (attendance.attendanceMark.absent) {
            attendance.shortMark = ATTENDANCE_ABSENT;
          } else {
            attendance.shortMark = ATTENDANCE_NOT_MARKED;
          }
          return attendance;
        })
      });
    case GET_ATTENDANCE_LIST_FAILURE:
      return Object.assign({}, state, {
        isGetAttendanceListInProgress: false,
        isGetAttendanceListFailed: true,
        attendanceList: undefined
      });
    default:
      return state;
  }
}

export default attendanceReducer;
