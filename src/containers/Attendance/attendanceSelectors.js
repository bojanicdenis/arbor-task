import { createSelector } from "reselect";

const selectAttendance = state => state.attendance;

const makeSelectIsGetAttendanceListInProgress = () =>
  createSelector(
    selectAttendance,
    attendanceState => attendanceState.isGetAttendanceListInProgress
  );

const makeSelectIsGetAttendanceListFailed = () =>
  createSelector(
    selectAttendance,
    attendanceState => attendanceState.isGetAttendanceListFailed
  );

const makeSelectAttendanceLists = () =>
  createSelector(
    selectAttendance,
    attendanceState => attendanceState.attendanceList
  );

export {
    selectAttendance,
    makeSelectIsGetAttendanceListInProgress,
    makeSelectIsGetAttendanceListFailed,
    makeSelectAttendanceLists,
};
