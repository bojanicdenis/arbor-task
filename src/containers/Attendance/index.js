import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import Attendance from "../../components/Attendance";
import Spinner from "../../components/common/Spinner";
import { getAttendanceListRequest } from "./attendanceActions";
import {
  makeSelectIsGetAttendanceListInProgress,
  makeSelectIsGetAttendanceListFailed,
  makeSelectAttendanceLists
} from "./attendanceSelectors";
import {
  ATTENDANCE_NOT_MARKED,
  ATTENDANCE_PRESENT,
  ATTENDANCE_LATE,
  ATTENDANCE_ABSENT
} from "./attendanceConstants";

const attendanceCounterDefaultState = {
  notMarked: 0,
  present: 0,
  late: 0,
  absent: 0
};

// Object that allows for easier access to attendanceCounter object properties that needs to be changed
const shortMarkToStateProp = {
  [ATTENDANCE_NOT_MARKED]: "notMarked`",
  [ATTENDANCE_PRESENT]: "present",
  [ATTENDANCE_LATE]: "late",
  [ATTENDANCE_ABSENT]: "absent"
};

class AttendancePage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      attendanceList: props.attendanceList,
      doneModalOpened: false,
      bundleActionModalOpened: false,
      attendanceCounter: { ...attendanceCounterDefaultState }
    };
  }

  componentDidMount() {
    this.props.dispatchGetAttendanceListRequest();
  }

  static getDerivedStateFromProps(nextProps, currentState) {
    if (nextProps.attendanceList !== currentState.attendanceList) {
      // Create new attendanceCounter object from newly arrived attendanceList
      let attendanceCounter = { ...attendanceCounterDefaultState };
      nextProps.attendanceList.forEach(({ shortMark }) => {
        switch (shortMark) {
          case ATTENDANCE_PRESENT:
            attendanceCounter.present++;
            break;
          case ATTENDANCE_LATE:
            attendanceCounter.late++;
            break;
          case ATTENDANCE_ABSENT:
            attendanceCounter.absent++;
            break;
          default:
            attendanceCounter.notMarked++;
        }
      });

      return {
        attendanceList: nextProps.attendanceList,
        attendanceCounter
      };
    }

    return currentState;
  }

  markAttendance = (attendeeId, mark) => () => {
    let newAttendanceList = [...this.state.attendanceList];
    let newAttendanceCounter = { ...this.state.attendanceCounter };
    let attendee = newAttendanceList.find(({ id }) => id === attendeeId);

    attendee.shortMark = mark;
    attendee.selected = false;
    newAttendanceCounter.notMarked--;
    newAttendanceCounter[shortMarkToStateProp[mark]]++;

    this.setState({
      attendanceList: newAttendanceList,
      attendanceCounter: newAttendanceCounter
    });
  };

  markSelected = attendeeId => {
    let newAttendanceList = [...this.state.attendanceList];
    let attendee = newAttendanceList.find(({ id }) => id === attendeeId);
    attendee.selected = !attendee.selected;

    this.setState({
      attendanceList: newAttendanceList
    });
  };

  resetAttendance = () => {
    let newAttendanceList = [...this.state.attendanceList];
    newAttendanceList.map(attendee => {
      attendee.shortMark = ATTENDANCE_NOT_MARKED;
      attendee.selected = false;
      return attendee;
    });

    this.setState({
      attendanceList: newAttendanceList,
      attendanceCounter: { ...attendanceCounterDefaultState }
    });
  };

  toggleDoneModal = () => {
    this.setState({ doneModalOpened: !this.state.doneModalOpened });
  };

  toggleBundleModal = () => {
    this.setState({
      bundleActionModalOpened: !this.state.bundleActionModalOpened,
      selectedAction: 0
    });
  };

  bundleActionSelect = param => e => {
    this.setState({ selectedAction: param });
  };

  bundleAction = () => {
    let mark = this.state.selectedAction;
    let newAttendanceList = [...this.state.attendanceList];
    let newAttendanceCounter = { ...this.state.attendanceCounter };
    // Mark attendees and increase/decrease corresponding attendance counters
    newAttendanceList.map(attendee => {
      if (attendee.selected) {
        if (mark !== ATTENDANCE_NOT_MARKED) {
          newAttendanceCounter.notMarked--;
          newAttendanceCounter[shortMarkToStateProp[mark]]++;
        } else {
          newAttendanceCounter.notMarked++;
          newAttendanceCounter[shortMarkToStateProp[attendee.shortMark]]--;
        }
        attendee.shortMark = mark;
        attendee.selected = false;
      }

      return attendee;
    });

    this.setState({
      attendanceList: newAttendanceList,
      attendanceCounter: newAttendanceCounter,
      bundleActionModalOpened: false
    });
  };

  render() {
    let {
      isGetAttendanceListInProgress,
      attendanceList,
      doneModalOpened,
      bundleActionModalOpened,
      attendanceCounter,
      selectedAction
    } = this.state;
    let isLoading = isGetAttendanceListInProgress || !attendanceList;

    if (isLoading) {
      return <Spinner />;
    }

    return (
      <Attendance
        attendanceList={attendanceList}
        attendanceCounter={attendanceCounter}
        doneModalOpened={doneModalOpened}
        bundleActionModalOpened={bundleActionModalOpened}
        selectedAction={selectedAction}
        bundleActionSelect={this.bundleActionSelect}
        bundleAction={this.bundleAction}
        markAttendance={this.markAttendance}
        markSelected={this.markSelected}
        toggleBundleModal={this.toggleBundleModal}
        toggleDoneModal={this.toggleDoneModal}
        resetAttendance={this.resetAttendance}
      />
    );
  }
}

AttendancePage.propTypes = {
  isGetAttendanceListInProgress: PropTypes.bool.isRequired,
  isGetAttendanceListFailed: PropTypes.bool.isRequired,
  attendanceList: PropTypes.array,
  dispatchGetAttendanceListRequest: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  isGetAttendanceListInProgress: makeSelectIsGetAttendanceListInProgress(),
  isGetAttendanceListFailed: makeSelectIsGetAttendanceListFailed(),
  attendanceList: makeSelectAttendanceLists()
});

const mapDispatchToPropsObj = {
  dispatchGetAttendanceListRequest: getAttendanceListRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToPropsObj
)(AttendancePage);
