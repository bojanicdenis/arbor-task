import {
    GET_ATTENDANCE_LIST_REQUEST,
    GET_ATTENDANCE_LIST_SUCCESS,
    GET_ATTENDANCE_LIST_FAILURE
} from './attendanceConstants';

export function getAttendanceListRequest() {
    return {
        type: GET_ATTENDANCE_LIST_REQUEST,
    };
}

export function getAttendanceListSuccess(attendanceList) {
    return {
        type: GET_ATTENDANCE_LIST_SUCCESS,
        attendanceList,
    };
}

export function getAttendanceListFailure(message) {
    return {
        type: GET_ATTENDANCE_LIST_FAILURE,
        message,
    };
}
