import string from "./en";
// Project is too small to include some large localization lib, but it is a good practice to separate strings into separate file
// that allows more easy future implementation
export default string;