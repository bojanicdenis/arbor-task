export default {
    // Header
    "frontendAssignement": "Frontend Interview Assignment",
    // Footer
    "madeBy": "Made by",
    // General
    "notMarked": "Unmarked",
    "present": "Present",
    "late": "Late",
    "absent": "Absent",
    "ok": "OK",
    // Attendance Counter
    "attendanceCount": "Attendance Count",
    // Attendance Main Buttons
    "markSelected": "Mark Selected",
    "resetAll": "Reset All",
    "done": "Done",
    // Bundel Action Modal
    "apply": "Apply",
    "bundleActions": "Bundle Actions",
    // Done Attendance Modal
    "finalAttendanceList": "Final Attendance List",
};